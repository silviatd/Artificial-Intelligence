# Tic Tac Toe

<img src="./tic-tac-toe.jpg" height="110">

# Min-Max Algorithm
Mini-max algorithm is a recursive or backtracking algorithm which is used in decision-making and game theory. It provides an optimal move for the player assuming that opponent is also playing optimally.