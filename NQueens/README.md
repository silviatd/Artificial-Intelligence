# NQueen
![image](./nqueen.png)

# Min conflict algorithms
Search algorithm or heuristic method to solve constraint satisfaction problems (CSP).

## Solution
![image](./img.png)

```
 private int[] queens;
```

```
 public void findSolution() {
        int i = 0;
        while (true) {
            int column = getColumnWithMaxConflicts();

            if (column == -1) {
                break;
            }

            int row = getRowWithMinConflicts(column);

            this.moveQueen(column, row);

            i++;

            if (i == MAX_ITERATIONS && !hasConflicts()) {
                //Random restart
                this.initializeBoard();
                i = 0;
            }
        }
    }

```