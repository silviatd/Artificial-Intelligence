# Sliding Blocks

<img src="./blocks.png" height="110">

# Iterative Deepening A*Algorithm
Iterative deepening A* (IDA*) is a graph traversal and path search algorithm that can find the shortest path between a designated start node and any member of a set of goal nodes in a weighted graph. It is a variant of iterative deepening depth-first search that borrows the idea to use a heuristic function to evaluate the remaining cost to get to the goal from the A* search algorithm.

# Manhattan Distance
Manhattan distances (sum of the vertical and horizontal distance) from the tiles to their goal positions

![image](./distance.png)
## Solution

```
    public Node findSolution() {
        int heuristic = this.manhattanSum(this.initialState, this.finalState);

        ArrayList<Node> path = new ArrayList<>();
        path.add(0, this.initialState);

        int threshold;
        do {
            threshold = limitedSearch(path, 0, heuristic);

            if (threshold == 0) {
                return path.get(path.size() - 1);
            }

            heuristic = threshold;
        } while (heuristic != Double.MAX_VALUE);

        return null;
    }

```